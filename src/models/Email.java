package models;

/**
 * Class representing simple email message
 */
public class Email {

    @SuppressWarnings("FieldCanBeLocal")
    private final String EMAIL_SAMPLE_TEXT = "Oto treść przykładowej wiadomości email przygotowanej na " +
            "potrzeby ćwiczenia związanego z wzorcem \"dekorator\".";

    private String emailText;

    public Email() {
        this.emailText = EMAIL_SAMPLE_TEXT;
    }

    public Email(String emailText) {
        this.emailText = emailText;
    }

    public String getEmailText() {
        return emailText;
    }
}
