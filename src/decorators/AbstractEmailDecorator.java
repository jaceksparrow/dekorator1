package decorators;

import models.Email;

/**
 * Base class for all email decorators
 */
public abstract class AbstractEmailDecorator extends Email {

    private Email email;

    public AbstractEmailDecorator(Email email) {
        this.email = email;
    }

    @Override
    public abstract String getEmailText();

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }
}
