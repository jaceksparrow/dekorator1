package decorators;

import models.Email;

/**
 * Email signature decorator class
 */
public class SignatureEmailDecorator extends AbstractEmailDecorator {

    @SuppressWarnings("FieldCanBeLocal")
    private final String EMAIL_SAMPLE_SIGNATURE = "\n\nPozdrawiam,\nAutor";

    public SignatureEmailDecorator(Email email) {
        super(email);
    }

    @Override
    public String getEmailText() {
        return buildEmailWithSignature(getEmail().getEmailText());
    }

    /**
     * Adds signature to given email
     *
     * @param email given email text
     * @return email with signature
     */
    private String buildEmailWithSignature(String email) {
        return email + EMAIL_SAMPLE_SIGNATURE;
    }
}
