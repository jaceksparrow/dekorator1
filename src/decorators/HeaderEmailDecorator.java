package decorators;

import models.Email;

/**
 * Email header decorator class
 */
public class HeaderEmailDecorator extends AbstractEmailDecorator {

    @SuppressWarnings("FieldCanBeLocal")
    private final String EMAIL_SAMPLE_HEADER = "Szanowny Czytelniku,\n";


    public HeaderEmailDecorator(Email email) {
        super(email);
    }

    @Override
    public String getEmailText() {
        return buildEmailWithHeader(getEmail().getEmailText());
    }

    /**
     * Adds header text to given email
     *
     * @param email given email text
     * @return email with header
     */
    private String buildEmailWithHeader(String email) {
        return EMAIL_SAMPLE_HEADER + email;
    }
}
